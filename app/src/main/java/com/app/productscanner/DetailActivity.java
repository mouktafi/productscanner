package com.app.productscanner;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.productscanner.databinding.ActivityDetailBinding;
import com.google.android.material.navigation.NavigationBarView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DetailActivity extends AppCompatActivity{

    SharedPreferences sharedPreferences;
    Fragment fragmentFlat;
    Fragment fragmentNotFound;
    Fragment fr;
    FragmentManager fm;
    FragmentTransaction tx;
    AppBarLayout appBarLayout;
    CollapsingToolbarLayout toolBarLayout;
    JSONObject jsonObject;
    JSONObject productObject;
    Bundle bundle,bundleFrag,bundleFragP,bundleFragG,bundleNotFound;
    FloatingActionButton fab;
    Drawable d;
    int status_int;
    int barcodeInt;
    String url,urlImg,barcode,spBarcode,product_str,
            prodName,prodBrand,
            prodCarbo,prodCarboSrv,prodCarboUnit,
            prodFiber,prodFiberSrv,prodFiberUnit,
            prodProteins,prodProteinsSrv,prodProteinsUnit,
            prodFats,prodFatsSrv,prodFatsUnit,
            prodSugars,prodSugarsSrv,prodSugarsUnit,
            prodSalt,prodSaltSrv,prodSaltUnit,
            prodIngredients,prodNutriscore,prodNova,prodEcoscore,ingredients_str;

    ImageView ivProdImage;
    BottomNavigationView bottomNavigation;

    private ActivityDetailBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolBarLayout = binding.toolbarLayout;

        appBarLayout = findViewById(R.id.app_bar);

        bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setOnNavigationItemSelectedListener(navListener);

        ivProdImage = findViewById(R.id.iv_prodimage);

        try {
            bundle = getIntent().getExtras();
            barcode = bundle.getString("barcode");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(barcode == null) {
            sharedPreferences = this.getSharedPreferences("barcode", Context.MODE_PRIVATE);
            barcode = sharedPreferences.getString("barcode", barcode);
            Log.i("SPSP", sharedPreferences.getString("barcode", barcode));

        }

        try {
            barcodeInt = Integer.parseInt(barcode);
        } catch (NumberFormatException ex) { // handle your exception
        }
        toolBarLayout.setTitle(barcode);
        url = "https://world.openfoodfacts.org/api/v0/product/"+barcode+".json";
        //url = "https://world.openfoodfacts.org/api/v0/product/4099200860800.json";

        try {
            DownloadTask task = new DownloadTask();
            task.execute(url);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        sharedPreferences = this.getSharedPreferences("barcode", Context.MODE_PRIVATE);
        sharedPreferences.edit().putString("barcode",barcode).apply();
    }

    public class DownloadTask extends AsyncTask<String, Void, String> {

        private ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(DetailActivity.this);
            dialog.setMessage("Loading, Please wait");
            dialog.setTitle("Connection to server");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... strings) {
            StringBuilder result = new StringBuilder();
            URL url;
            HttpURLConnection urlConnection;

            try {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);

                int data = reader.read();

                while (data != -1) {
                    char cur = (char)data;
                    result.append(cur);
                    data = reader.read();
                }
                return result.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                jsonObject = new JSONObject(result);

                status_int = jsonObject.optInt("status");
                product_str = jsonObject.optString("product");

                productObject = new JSONObject(product_str);


                //Log.i("status", String.valueOf(status_int));
                //product_str = jsonObject.optString("product");


                //Log.i("status_str", status_int);
                //JSONArray productArray = new JSONArray(product_str);

                //JSONObject statusObject = new JSONObject(status_str);
                //Log.i("productArray", String.valueOf(productArray));
                //String statusVerbose = statusObject.getString("status_verbose");
                prodName = productObject.optString("product_name");
                prodBrand = productObject.optString("brands");
                prodIngredients = productObject.optString("ingredients_text");

                ingredients_str = productObject.optString("ingredients");
                //JSONObject ingObj = new JSONObject(ingredients_str);

                String prodNutriments = productObject.optString("nutriments");
                prodNutriscore = productObject.optString("nutriscore_grade");
                prodNova = productObject.optString("nova_group");
                prodEcoscore = productObject.optString("ecoscore_grade");
                JSONObject nutrimentsObject = new JSONObject(prodNutriments);
                prodCarbo = nutrimentsObject.optString("carbohydrates");
                prodCarboSrv = nutrimentsObject.optString("carbohydrates_serving");
                prodCarboUnit = nutrimentsObject.optString("carbohydrates_unit");
                prodFiber = nutrimentsObject.optString("fiber");
                prodFiberSrv = nutrimentsObject.optString("fiber_serving");
                prodFiberUnit = nutrimentsObject.optString("fiber_unit");
                prodProteins = nutrimentsObject.optString("proteins");
                prodProteinsSrv = nutrimentsObject.optString("proteins_serving");
                prodProteinsUnit = nutrimentsObject.optString("proteins_unit");
                prodFats = nutrimentsObject.optString("fat");
                prodFatsSrv = nutrimentsObject.optString("fat_serving");
                prodFatsUnit = nutrimentsObject.optString("fat_unit");
                prodSugars = nutrimentsObject.optString("sugars");
                prodSugarsSrv = nutrimentsObject.optString("sugars_serving");
                prodSugarsUnit = nutrimentsObject.optString("sugars_unit");
                prodSalt = nutrimentsObject.optString("salt");
                prodSaltSrv = nutrimentsObject.optString("salt_serving");
                prodSaltUnit = nutrimentsObject.optString("salt_unit");

                String prodImgUrl = productObject.optString("image_url");


                Log.i("prodName", prodName);
                Log.i("prodImgUrl", prodImgUrl);
                //Log.i("prodIngredients", prodIngredients);
                Log.i("prodNutriscore", prodNutriscore);
                Log.i("status_int2", String.valueOf(status_int));



                urlImg = prodImgUrl;
                new FetchImage(urlImg).start();

               /*tvProdName.setText(prodName);
                if (prodBrand.isEmpty()) {
                    tvProdBrand.setVisibility(View.GONE);
                }else{
                    tvProdBrand.setText(prodBrand);
                }
                //tvProdIngredients.setText(prodIngredients);
                if (prodCarbo.isEmpty()) {
                    tvCarbo.setText(R.string.product_no_data);
                    tvCarbo.setTextSize(14);
                    tvCarbo.setTypeface(null, Typeface.NORMAL);
                }else{
                    tvCarbo.setText(String.format("%s%s", prodCarbo, prodCarboUnit));
                }
                if (prodFiber.isEmpty()) {
                    tvFiber.setText(R.string.product_no_data);
                    tvFiber.setTextSize(16);
                    tvFiber.setTypeface(null, Typeface.NORMAL);
                }else {
                    tvFiber.setText(String.format("%s%s", prodFiber, prodFiberUnit));
                }
                if (prodProteins.isEmpty()) {
                    tvProteins.setText(R.string.product_no_data);
                    tvProteins.setTextSize(14);
                    tvProteins.setTypeface(null, Typeface.NORMAL);
                }else {
                    tvProteins.setText(String.format("%s%s", prodProteins, prodProteinsUnit));
                }
                if (prodFats.isEmpty()) {
                    tvFats.setText(R.string.product_no_data);
                    tvFats.setTextSize(14);
                    tvFats.setTypeface(null, Typeface.NORMAL);
                }else{
                    tvFats.setText(String.format("%s%s", prodFats, prodFatUnit));
                }
                if (prodSugars.isEmpty()) {
                    tvSugars.setText(R.string.product_no_data);
                    tvSugars.setTextSize(14);
                    tvSugars.setTypeface(null, Typeface.NORMAL);
                }else {
                    tvSugars.setText(String.format("%s%s", prodSugars, prodSugarsUnit));
                }
                if (prodSalt.isEmpty()) {
                    tvSalt.setText(R.string.product_no_data);
                    tvSalt.setTextSize(14);
                    tvSalt.setTypeface(null, Typeface.NORMAL);
                }else {
                    tvSalt.setText(String.format("%s%s", prodSalt, prodSaltUnit));
                }*/

            } catch (JSONException e) {
                e.printStackTrace();
            }
            openFragmentFlat();
            dialog.cancel();

          /*  btnGraphs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(DetailActivity.this, GraphsActivity.class);
                    i.putExtra("prodCarbo",prodCarbo);
                    i.putExtra("prodFiber",prodFiber);
                    i.putExtra("prodProteins",prodProteins);
                    i.putExtra("prodFats",prodFats);
                    i.putExtra("prodSugars",prodSugars);
                    i.putExtra("prodSalt",prodSalt);
                    startActivity(i);
                }
            });*/
        }
    }

    public void openFragmentFlat() {
        Log.i("status_int", String.valueOf(status_int));
        if (status_int == 0) {
            bottomNavigation.setVisibility(View.GONE);
            ivProdImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_fastfood_24));
            ivProdImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
            bundleNotFound = new Bundle();
            bundleNotFound.putInt("statusInt", status_int);
            fragmentNotFound = new NotFoundFragment();
            fragmentNotFound.setArguments(bundleNotFound);
            fm = getSupportFragmentManager();
            tx = fm.beginTransaction();
            tx.add(R.id.fl_frame, fragmentNotFound);
        }else {
            bundleProdFacts();
            fragmentFlat = new FlatFactsFragment();
            fragmentFlat.setArguments(bundleFrag);
            fm = getSupportFragmentManager();
            tx = fm.beginTransaction();
            tx.add(R.id.fl_frame, fragmentFlat);
        }
        tx.commit();
    }

    public void bundleProdFacts(){
        bundleFrag = new Bundle();
        bundleFrag.putString("prodName", prodName);
        bundleFrag.putString("prodBrand", prodBrand);
        bundleFrag.putString("prodCarbo", prodCarbo);
        bundleFrag.putString("prodCarboUnit", prodCarboUnit);
        bundleFrag.putString("prodFiber", prodFiber);
        bundleFrag.putString("prodFiberUnit", prodFiberUnit);
        bundleFrag.putString("prodProteins", prodProteins);
        bundleFrag.putString("prodProteinsUnit", prodProteinsUnit);
        bundleFrag.putString("prodFats", prodFats);
        bundleFrag.putString("prodFatsUnit", prodFatsUnit);
        bundleFrag.putString("prodSugars", prodSugars);
        bundleFrag.putString("prodSugarsUnit", prodSugarsUnit);
        bundleFrag.putString("prodSalt", prodSalt);
        bundleFrag.putString("prodSaltUnit", prodSaltUnit);
    }
    public void bundleProdFactsSrv(){
        bundleFragP = new Bundle();
        bundleFragP.putString("prodNameSrv", prodName);
        bundleFragP.putString("prodBrandSrv", prodBrand);
        bundleFragP.putString("prodCarboSrv", prodCarboSrv);
        bundleFragP.putString("prodCarboUnit", prodCarboUnit);
        bundleFragP.putString("prodFiberSrv", prodFiberSrv);
        bundleFragP.putString("prodFiberUnit", prodFiberUnit);
        bundleFragP.putString("prodProteinsSrv", prodProteinsSrv);
        bundleFragP.putString("prodProteinsUnit", prodProteinsUnit);
        bundleFragP.putString("prodFatsSrv", prodFatsSrv);
        bundleFragP.putString("prodFatsUnit", prodFatsUnit);
        bundleFragP.putString("prodSugarsSrv", prodSugarsSrv);
        bundleFragP.putString("prodSugarsUnit", prodSugarsUnit);
        bundleFragP.putString("prodSaltSrv", prodSaltSrv);
        bundleFragP.putString("prodSaltUnit", prodSaltUnit);
    }
    public void bundleProdFactsGraph(){
        bundleFragG = new Bundle();
        bundleFragG.putString("prodCarboG", prodCarbo);
        bundleFragG.putString("prodFiberG", prodFiber);
        bundleFragG.putString("prodProteinsG", prodProteins);
        bundleFragG.putString("prodFatsG", prodFats);
        bundleFragG.putString("prodSugarsG", prodSugars);
        bundleFragG.putString("prodSaltG", prodSalt);
    }

    private final BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()){

                case R.id.btn_flat_facts:
                    bundleProdFacts();
                    fr = new FlatFactsFragment();
                    fr.setArguments(bundleFrag);
                    break;

                case R.id.btn_port_facts:
                    bundleProdFactsSrv();
                    fr = new PortionFactsFragment();
                    fr.setArguments(bundleFragP);
                    break;

                case R.id.btn_ingredients_bottom:
                    fr = new IngredientsFragment();
                    break;

                case R.id.btn_graphs_bottom:
                    bundleProdFactsGraph();
                    fr = new GraphFragment();
                    fr.setArguments(bundleFragG);
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fl_frame,fr).commit();

            return true;
        }
    };

    class FetchImage extends Thread {

        String URL;
        Bitmap bitmap;


        FetchImage(String URL) {
            this.URL = URL;
        }
        @Override
        public void run() {

            InputStream inputStream;
            try {
                inputStream = new URL(URL).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            d = new BitmapDrawable(getResources(), bitmap);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ivProdImage.setImageDrawable(d);
                    toolBarLayout.setTitle(prodName);
                    fab = binding.fab;

                    if (prodIngredients .isEmpty()) {
                        fab.setVisibility(View.GONE);
                    }
                    fab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(DetailActivity.this,IngredientsActivity.class);
                            i.putExtra("prodName", prodName);
                            i.putExtra("prodIngredients", prodIngredients);
                            i.putExtra("prodNutriscore",prodNutriscore);
                            i.putExtra("prodNova",prodNova);
                            i.putExtra("prodEcoscore",prodEcoscore);
                            i.putExtra("ingredients",ingredients_str);
                            startActivity(i);
                        }
                    });
                }
            });
        }
    }
}