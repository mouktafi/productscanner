package com.app.productscanner;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class UserProfileActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    SharedPreferences spUserSex,spUserWeight,spUserBirthdate,spUserFit,spUserCalories;
    int userSex,userWeightInt;
    String userWeightStr,userBirthdateStr,userFitStr,userCaloriesStr;
    ImageView ivUserSex;
    TextView tvUserWeight,tvUserAge,tvUserFit,tvUserCalories;
    DateTimeFormatter formatter;
    long userAgeLng;
    double userBasalMet,userLaf,userCaloriesDb;
    Calendar cal;
    DateFormat sdf;
    LocalDate todayDate;
    FloatingActionButton fabEditProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        ivUserSex = findViewById(R.id.iv_user_sex);
        tvUserWeight = findViewById(R.id.tv_user_weight);
        tvUserAge = findViewById(R.id.tv_user_age);
        tvUserFit = findViewById(R.id.tv_user_fit);
        tvUserCalories = findViewById(R.id.tv_user_calories);
        fabEditProfile = findViewById(R.id.fab_edit_profile);

    }

   @RequiresApi(api = Build.VERSION_CODES.O)
   @Override
    protected void onResume() {
        try {
            GetSharedPreferences();
            AgeCalc();
            CaloriesCalc();
            SetDataToUserProfile();

            super.onResume();
        } catch (Exception e) {
            e.printStackTrace();
        }
   }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        getMenuInflater().inflate(R.menu.user_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit_user:
                Intent i = new Intent(UserProfileActivity.this,UserEditActivity.class);
                startActivity(i);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSharedPreferences("userSex", MODE_PRIVATE).registerOnSharedPreferenceChangeListener(this);
        getSharedPreferences("userWeight", MODE_PRIVATE).registerOnSharedPreferenceChangeListener(this);
        getSharedPreferences("userBirthdateStr", MODE_PRIVATE).registerOnSharedPreferenceChangeListener(this);
        getSharedPreferences("userFitStr", MODE_PRIVATE).registerOnSharedPreferenceChangeListener(this);
        getSharedPreferences("userCalories", MODE_PRIVATE).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onStop() {
        getSharedPreferences("userSex", MODE_PRIVATE).unregisterOnSharedPreferenceChangeListener(this);
        getSharedPreferences("userWeight", MODE_PRIVATE).unregisterOnSharedPreferenceChangeListener(this);
        getSharedPreferences("userBirthdateStr", MODE_PRIVATE).unregisterOnSharedPreferenceChangeListener(this);
        getSharedPreferences("userFitStr", MODE_PRIVATE).unregisterOnSharedPreferenceChangeListener(this);
        getSharedPreferences("userCalories", MODE_PRIVATE).unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {

        try {
            GetSharedPreferences();
            AgeCalc();
            CaloriesCalc();
            SetDataToUserProfile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void GetSharedPreferences() {

            spUserSex = this.getSharedPreferences("userSex", Context.MODE_PRIVATE);
            userSex = spUserSex.getInt("userSex", 0);

            spUserWeight = this.getSharedPreferences("userWeight", Context.MODE_PRIVATE);
            userWeightInt = spUserWeight.getInt("userWeight", userWeightInt);

            spUserBirthdate = this.getSharedPreferences("userBirthdateStr", Context.MODE_PRIVATE);
            userBirthdateStr = spUserBirthdate.getString("userBirthdateStr", userBirthdateStr);

            spUserFit = this.getSharedPreferences("userFitStr", Context.MODE_PRIVATE);
            userFitStr = spUserFit.getString("userFitStr", userFitStr);

            spUserCalories = this.getSharedPreferences("userCalories", Context.MODE_PRIVATE);
            userCaloriesDb = spUserCalories.getInt("UserCalories", (int) userCaloriesDb);

    }

    private void SetDataToUserProfile() {
            if (userSex == 1) {
                ivUserSex.setImageResource(R.drawable.ic_baseline_male_24);
            } else if (userSex == 2) {
                ivUserSex.setImageResource(R.drawable.ic_baseline_female_24);
            } else if (userSex == 0) {
                ivUserSex.setImageResource(R.drawable.ic_baseline_pest_control_rodent_24);
            }
            userWeightStr = String.valueOf(userWeightInt);
            tvUserWeight.setText(userWeightStr);
            tvUserAge.setText(String.valueOf(userAgeLng));
            tvUserFit.setText(userFitStr);
            userCaloriesStr = String.valueOf(Math.round(userCaloriesDb));
            tvUserCalories.setText(userCaloriesStr);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void AgeCalc() {
        try {
            //cal = Calendar.getInstance();
            //sdf = new SimpleDateFormat("dd MM yyyy");

            //todayDate = sdf.format(cal.getTime());

            todayDate = LocalDate.now();
            Log.i("LDtodayDate", String.valueOf(todayDate));

            String inputString1 = userBirthdateStr;
            //String inputString2 = todayDate;

            formatter = DateTimeFormatter.ofPattern("dd MM yyyy");
            LocalDate date1 = LocalDate.parse(inputString1, formatter);
            //LocalDate date2 = LocalDate.parse(inputString2, formatter);
            userAgeLng = Period.between(date1, todayDate).getYears();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void BasalMetCalcF()  {

        if ((userAgeLng >= 18) && (userAgeLng <= 29)) {
            userBasalMet = 14.7 * userWeightInt + 496;
        }else if ((userAgeLng >= 30) && (userAgeLng <= 59)) {
            userBasalMet = 8.7 * userWeightInt + 829;
        }else if ((userAgeLng >= 60) && (userAgeLng <= 74)) {
            userBasalMet = 9.2 * userWeightInt + 688;
        }else if (userAgeLng >= 75) {
            userBasalMet = 9.8 * userWeightInt + 624;
        }
        Log.i("basalF", String.valueOf(userBasalMet));
    }

    private void BasalMetCalcM()  {

        if ((userAgeLng >= 18) && (userAgeLng <= 29)) {
            userBasalMet = 15.3 * userWeightInt + 679;
        }else if ((userAgeLng >= 30) && (userAgeLng <= 59)) {
            userBasalMet = 11.6 * userWeightInt + 879;
        }else if ((userAgeLng >= 60) && (userAgeLng <= 74)) {
            userBasalMet = 11.9 * userWeightInt + 700;
        }else if (userAgeLng >= 75) {
            userBasalMet = 8.4 * userWeightInt + 819;
        }
        Log.i("basalM", String.valueOf(userBasalMet));
    }

    private void LafCalcF()  {

        if ((userAgeLng >= 18) && (userAgeLng <= 59)) {
            switch (userFitStr) {
                case "Leggera":
                    userLaf = 1.56;
                    break;
                case "Moderata":
                    userLaf = 1.64;
                    break;
                case "Intensa":
                    userLaf = 1.82;
                    break;
            }
        }else if ((userAgeLng >= 60) && (userAgeLng <= 74)) {
            userLaf = 1.56;
        }else if (userAgeLng >= 75) {
            userLaf = 1.56;
        }
        Log.i("lafF", String.valueOf(userLaf));
    }

    private void LafCalcM()  {

        if ((userAgeLng >= 18) && (userAgeLng <= 59)) {
            if (userFitStr.equals("Leggera")) {
                userLaf = 1.55;
            }else if(userFitStr.equals("Moderata")) {
                userLaf = 1.78;
            }
            else if(userFitStr.equals("Intensa")) {
                userLaf = 2.10;
            }
        }else if ((userAgeLng >= 60) && (userAgeLng <= 74)) {
            userLaf = 1.51;
        }else if (userAgeLng >= 75) {
            userLaf = 1.51;
        }
        Log.i("lafM", String.valueOf(userLaf));
    }

    private void CaloriesCalc() {
        if (userSex == 1) {
            try {
                BasalMetCalcM();
                LafCalcM();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.i("MMM1", String.valueOf(userBasalMet));
        } else if (userSex == 2) {
            try {
                BasalMetCalcF();
                LafCalcF();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.i("MMM2", String.valueOf(userBasalMet));
        }
        userCaloriesDb = userBasalMet*userLaf;
        //spUserCalories = this.getSharedPreferences("UserCalories", Context.MODE_PRIVATE);
        spUserCalories.edit().putInt("userCalories", (int) userCaloriesDb).apply();
        //Log.i("calories", String.valueOf(userCaloriesDb));
    }
    public void goEditProfile(View view) {
        Intent i = new Intent(UserProfileActivity.this,UserEditActivity.class);
        startActivity(i);
    }
}