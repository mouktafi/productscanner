package com.app.productscanner;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

public class GraphsActivity extends AppCompatActivity {

    BarChart barChart;
    Bundle bundle;
    double prodCarboDb,prodFiberDb,prodProteinsDb,prodFatsDb,prodSugarsDb,prodSaltDb;
    String prodCarbo,prodFiber,prodProteins,prodFats,prodSugars,prodSalt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graphs);

        //barChart = findViewById(R.id.chart);
        bundle = getIntent().getExtras();

        try {
            prodCarbo = bundle.getString("prodCarboG");
            if (!prodCarbo.isEmpty()){
                prodCarboDb = Double.parseDouble(prodCarbo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            prodFiber = bundle.getString("prodFiberSrvG");
            if (!prodFiber.isEmpty()){
                prodFiberDb = Double.parseDouble(prodFiber);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            prodProteins = bundle.getString("prodProteinsG");
            if (!prodProteins.isEmpty()){
                prodProteinsDb = Double.parseDouble(prodProteins);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            prodFats = bundle.getString("prodFatsG");
            if (!prodFats.isEmpty()){
                prodFatsDb = Double.parseDouble(prodFats);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            prodSugars = bundle.getString("prodSugarsG");
            if (!prodSugars.isEmpty()){
                prodSugarsDb = Double.parseDouble(prodSugars);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            prodSalt = bundle.getString("prodSaltG");
            if (!prodSalt.isEmpty()){
                prodSaltDb = Double.parseDouble(prodSalt);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<BarEntry> yNutrients = new ArrayList<>();
       /* yNutrients.add(new BarEntry(1, 32));
        yNutrients.add(new BarEntry(2,64));
        yNutrients.add(new BarEntry(3, 128));*/
        int i = 1;
        if (!prodCarbo.isEmpty()) {
            yNutrients.add(new BarEntry(i, (float) prodCarboDb));
            i++;
        }

        if (!prodFiber.isEmpty()) {
            yNutrients.add(new BarEntry(i, (float) prodFiberDb));
            i++;
        }

        if (!prodProteins.isEmpty()) {
            yNutrients.add(new BarEntry(i, (float) prodProteinsDb));
            i++;
        }
        if (!prodFats.isEmpty()) {
            yNutrients.add(new BarEntry(i,(float) prodFatsDb));
            i++;
        }
        if (!prodSugars.isEmpty()) {
            yNutrients.add(new BarEntry(i,(float) prodSugarsDb));
            i++;
        }
        if (!prodSalt.isEmpty()) {
            yNutrients.add(new BarEntry(i, (float) prodSaltDb));
        }

        BarDataSet barDataSet = new BarDataSet(yNutrients,"Valori nutrizionali");
        barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
       /* barDataSet.setColors(
                            Color.parseColor("#cc0000"),
                            Color.parseColor("#339933"),
                            Color.parseColor("#ff5050"),
                            Color.parseColor("#ff7e29"),
                            Color.parseColor("#ffccff"),
                            Color.parseColor("#0099ff")
                            );*/
        barDataSet.setValueTextColor(Color.BLACK);
        barDataSet.setValueTextSize(16f);

        BarData barData = new BarData(barDataSet);
        barChart.setFitBars(true);
        barChart.setData(barData);
        barChart.getDescription().setText("barchart");
        barChart.animateY(2000);
    }


}