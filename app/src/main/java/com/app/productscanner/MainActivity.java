package com.app.productscanner;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.PopupMenu;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.app.productscanner.databinding.ActivityMainBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    Float translationYaxis = 100f;
    Boolean menuOpen = false;
    FloatingActionButton fab_scan_main,fab_scan,fab_last_scan;
    MenuItem profile;

    OvershootInterpolator interpolator = new OvershootInterpolator();

    @Override
    protected void onPause() {
        super.onPause();
        CloseMenu();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);

        profile = findViewById(R.id.menu_profile);
        
        showScanMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_profile:
                Intent i = new Intent(MainActivity.this,UserProfileActivity.class);
                startActivity(i);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showScanMenu() {

        fab_scan_main = findViewById(R.id.fab_main_scan);
        fab_scan = findViewById(R.id.fab_scan);
        fab_last_scan = findViewById(R.id.fab_last_scan);

        fab_last_scan.setAlpha(0f);
        fab_scan.setAlpha(0f);

        fab_last_scan.setTranslationY(translationYaxis);
        fab_scan.setTranslationY(translationYaxis);

        fab_scan_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (menuOpen) {
                    CloseMenu();
                }else{
                    OpenMenu();
                }
            }
        });

        fab_last_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,DetailActivity.class);
                startActivity(i);
            }
        });
        fab_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,CodeScannerActivity.class);
                startActivity(i);
            }
        });
    }

    private void OpenMenu() {
        menuOpen =!menuOpen;
        fab_last_scan.animate().translationY(0f).alpha(1f).setInterpolator(interpolator).setDuration(300).start();
        fab_scan.animate().translationY(0f).alpha(1f).setInterpolator(interpolator).setDuration(300).start();
    }

    private void CloseMenu() {
        menuOpen =!menuOpen;
        fab_last_scan.animate().translationY(translationYaxis).alpha(0f).setInterpolator(interpolator).setDuration(300).start();
        fab_scan.animate().translationY(translationYaxis).alpha(0f).setInterpolator(interpolator).setDuration(300).start();
    }
}