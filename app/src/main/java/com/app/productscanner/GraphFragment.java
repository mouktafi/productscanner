package com.app.productscanner;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class GraphFragment extends Fragment {

    TextView textView;
    //BarDataSet barDataSet;
    RadarDataSet dataSet;
    //BarData barData;
    //BarChart barChart;
    RadarChart radarChart;
    double prodCarboDb,prodFiberDb,prodProteinsDb,prodFatsDb,prodSugarsDb,prodSaltDb;
    String prodCarbo,prodFiber,prodProteins,prodFats,prodSugars,prodSalt;
    String[] labels = {"Carboidrati","Fibre","Proteine","Grassi","Zuccheri","Sale"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_graph, container, false);
        //barChart = view.findViewById(R.id.chart);
        radarChart = view.findViewById(R.id.chartRadar);
        //textView = view.findViewById(R.id.textView);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assert getArguments() != null;
       try {
            prodCarbo = getArguments().getString("prodCarboG");
           //textView.setText(prodCarbo);
            if (!prodCarbo.isEmpty()){
                prodCarboDb = Double.parseDouble(prodCarbo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            prodFiber = getArguments().getString("prodFiberG");
            if (!prodFiber.isEmpty()){
                prodFiberDb = Double.parseDouble(prodFiber);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            prodProteins = getArguments().getString("prodProteinsG");
            if (!prodProteins.isEmpty()){
                prodProteinsDb = Double.parseDouble(prodProteins);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            prodFats = getArguments().getString("prodFatsG");
            if (!prodFats.isEmpty()){
                prodFatsDb = Double.parseDouble(prodFats);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            prodSugars = getArguments().getString("prodSugarsG");
            if (!prodSugars.isEmpty()){
                prodSugarsDb = Double.parseDouble(prodSugars);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            prodSalt = getArguments().getString("prodSaltG");
            if (!prodSalt.isEmpty()){
                prodSaltDb = Double.parseDouble(prodSalt);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //ArrayList<BarEntry> yNutrients = new ArrayList<>();


        /*int i = 1;
        if (!prodCarbo.isEmpty()) {
            yNutrients.add(new BarEntry(i, (float) prodCarboDb));
            i++;
        }

        if (!prodFiber.isEmpty()) {
            yNutrients.add(new BarEntry(i, (float) prodFiberDb));
            i++;
        }

        if (!prodProteins.isEmpty()) {
            yNutrients.add(new BarEntry(i, (float) prodProteinsDb));
            i++;
        }
        if (!prodFats.isEmpty()) {
            yNutrients.add(new BarEntry(i,(float) prodFatsDb));
            i++;
        }
        if (!prodSugars.isEmpty()) {
            yNutrients.add(new BarEntry(i,(float) prodSugarsDb));
            i++;
        }
        if (!prodSalt.isEmpty()) {
            yNutrients.add(new BarEntry(i, (float) prodSaltDb));
        }*/

        dataSet = new RadarDataSet(dataValue(),"Nutriments");
        dataSet.setColors(Color.RED);
        RadarData data = new RadarData();
        data.addDataSet(dataSet);

        XAxis radarXAxis = radarChart.getXAxis();

        radarXAxis.setValueFormatter(new IndexAxisValueFormatter(labels));
        radarChart.setData(data);
        radarChart.animateY(2000);
        radarChart.invalidate();

        Legend legend = radarChart.getLegend();
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);

       /* barDataSet = new BarDataSet(yNutrients,"Valori nutrizionali");
        barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);

        barDataSet.setValueTextColor(Color.BLACK);
        barDataSet.setValueTextSize(16f);

        XAxis xAxis = barChart.getXAxis();
        Legend legend = barChart.getLegend();
        barData = new BarData(barDataSet);
        barChart.setFitBars(true);
        barChart.setData(barData);
        barChart.getDescription().setText("barchart");
        barChart.animateY(2000);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        legend.setForm(Legend.LegendForm.LINE);

        legend.setTextSize(18f);

        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);

        legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);*/
    }
    private ArrayList<RadarEntry> dataValue(){
        ArrayList<RadarEntry> radarNutriments = new ArrayList<>();

        radarNutriments.add(new RadarEntry((float) prodCarboDb));
        radarNutriments.add(new RadarEntry((float) prodFiberDb));
        radarNutriments.add(new RadarEntry((float) prodProteinsDb));
        radarNutriments.add(new RadarEntry((float) prodFatsDb));
        radarNutriments.add(new RadarEntry((float) prodSugarsDb));
        radarNutriments.add(new RadarEntry((float) prodSaltDb));
        return radarNutriments;
    }


}