package com.app.productscanner;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ResultCodeActivity extends AppCompatActivity {

    String url;
    TextView tvBarcode;
    Bundle bundle;
    String barcode;
    int barcodeInt;
    TextView tvProdName;
    TextView tvProdBrand;
    TextView tvProdIngredients;
    TextView tvCarbo;
    TextView tvCarboUnit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_code);

        //tvBarcode = findViewById(R.id.tv_barcode_result);
        tvProdName = findViewById(R.id.tv_prodNameP);
        tvProdBrand = findViewById(R.id.tv_prodBrandP);
        //tvProdIngredients = findViewById(R.id.tv_prodIngredients);
        tvCarbo = findViewById(R.id.tv_carbo);
        //tvCarboUnit = findViewById(R.id.tv_carbo_unit);
        //tvCodePoints = findViewById(R.id.tv_code_points);
        bundle = getIntent().getExtras();
        assert bundle != null;
        barcode = bundle.getString("barcode");
        try {
            barcodeInt = Integer.parseInt(barcode);
        } catch (NumberFormatException ex) { // handle your exception
        }
        setTitle("COD: "+barcode);

        //tvBarcode.setText(barcode);


/*        assert barcode != null;
        if (barcode.startsWith("80")) {
            tvCodePoints.setText("100");
        } else if (barcode.startsWith("50")) {
            tvCodePoints.setText("50");
        } else {
            tvCodePoints.setText("0");
        }*/
        /*assert barcode != null;
        Log.i("bar", barcode);*/
        url = "https://world.openfoodfacts.org/api/v0/product/"+barcode+".json";
        //url = "https://world.openfoodfacts.org/api/v0/product/4099200860800.json";


        try {
            DownloadTask task = new DownloadTask();
            //http://api.openweathermap.org/data/2.5/forecast?q=Torino&units=metric&APPID=c26c578f70df09278c782b96e34e31ed
            //String APPID = "c26c578f70df09278c782b96e34e31ed";
            task.execute(url);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

/*    @Override
    protected void onPause() {
        super.onPause();
        String codePoints = String.valueOf(tvCodePoints.getText());
        int codePointsInt = Integer.parseInt(codePoints);
        SharedPreferences sp = this.getSharedPreferences("codePoints", Context.MODE_PRIVATE);
        sp.edit().putInt("codePoints",codePointsInt).apply();
        int spCodePoints = sp.getInt("codePoints",0);
        Log.i("onPause", String.valueOf(spCodePoints));
    }*/

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public class DownloadTask extends AsyncTask<String, Void, String> {

        private ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(ResultCodeActivity.this);
            dialog.setMessage("Loading, Please wait");
            dialog.setTitle("Connection to server");
            dialog.show();
            dialog.setCancelable(false);
        }


        @Override
        protected String doInBackground(String... strings) {
            StringBuilder result = new StringBuilder();
            URL url;
            HttpURLConnection urlConnection;

            try {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);

                int data = reader.read();

                while (data != -1) {
                    char cur = (char)data;
                    result.append(cur);
                    data = reader.read();
                }
                return result.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.i("JSON", result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                //Log.i("JSONObject", String.valueOf(jsonObject));
                String product_str = jsonObject.getString("product");
                //Log.i("product_str", product_str);
                //JSONArray productArray = new JSONArray(product_str);
                JSONObject productObject = new JSONObject(product_str);
                //Log.i("productArray", String.valueOf(productArray));
                String prodName = productObject.getString("product_name");
                String prodBrand = productObject.getString("brands");
                String prodIngredients = productObject.getString("ingredients_text");
                String prodNutriments = productObject.getString("nutriments");
                JSONObject nutrimentsObject = new JSONObject(prodNutriments);
                String prodCarbo = nutrimentsObject.getString("carbohydrates");
                String prodCarboUnit = nutrimentsObject.getString("carbohydrates_unit");
                Log.i("prodName", prodName);
                Log.i("prodBrand", prodBrand);
                Log.i("prodIngredients", prodIngredients);
                Log.i("prodNutriments", prodNutriments);
                Log.i("prodCarbo", prodCarbo);
                Log.i("prodCarboUnit", prodCarboUnit);
                tvProdName.setText(prodName);
                tvProdBrand.setText(prodBrand);
                tvProdIngredients.setText(prodIngredients);
                tvCarbo.setText(prodCarbo);
                tvCarboUnit.setText(prodCarboUnit);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            dialog.cancel();
        }
    }
}