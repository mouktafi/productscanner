package com.app.productscanner.models;

public class Ingredient {

   private final String name;
   private final String date;

   public Ingredient(String name, String date) {
      this.name = name;
      this.date = date;
   }

   public String getName() {
      return name;
   }

   public String getDate() {
      return date;
   }

}

