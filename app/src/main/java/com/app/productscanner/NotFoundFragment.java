package com.app.productscanner;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class NotFoundFragment extends Fragment {

    int status_int;
    TextView tvProdStatus;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        status_int = getArguments().getInt("statusInt");
        Log.i("status_inDt", String.valueOf(status_int));
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_not_found, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvProdStatus = view.findViewById(R.id.tv_prodStatus);

        if (status_int == 0) {
            //ivProdImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_warning_24));
            tvProdStatus.setText("Prodotto non trovato");
        }
    }
}