package com.app.productscanner;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class PortionFactsFragment extends Fragment {

    String url,urlImg,barcode,spBarcode,prodName,prodBrand,prodCarbo,prodCarboUnit,prodFiber,prodFiberUnit,prodProteins,prodProteinsUnit,prodFats,prodFatsUnit,prodSugars,prodSugarsUnit,prodSalt,prodSaltUnit,prodIngredients,prodNutriscore,prodNova,prodEcoscore,ingredients_str;
    ConstraintLayout clStatus,clDetail;
    TextView tvStatus,tvProdNameP,tvProdBrandP,tvCarboP,tvFiberP,tvFatsP,tvProteinsP,tvSugarsP,tvSaltP;
    ImageView ivProdImage;
    Button btnGraphs,btnAltFacts;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        prodName = getArguments().getString("prodNameSrv");
        prodBrand = getArguments().getString("prodBrandSrv");
        prodCarbo = getArguments().getString("prodCarboSrv");
        prodCarboUnit = getArguments().getString("prodCarboUnit");
        prodFiber = getArguments().getString("prodFiberSrv");
        prodFiberUnit = getArguments().getString("prodFiberUnit");
        prodProteins = getArguments().getString("prodProteinsSrv");
        prodProteinsUnit = getArguments().getString("prodProteinsUnit");
        prodFats = getArguments().getString("prodFatsSrv");
        prodFatsUnit = getArguments().getString("prodFatsUnit");
        prodSugars = getArguments().getString("prodSugarsSrv");
        prodSugarsUnit = getArguments().getString("prodSugarsUnit");
        prodSalt = getArguments().getString("prodSaltSrv");
        prodSaltUnit = getArguments().getString("prodSaltUnit");

        return inflater.inflate(R.layout.fragment_portion_facts, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //clStatus = requireActivity().findViewById(R.id.cl_status;
        clDetail = requireActivity().findViewById(R.id.cl_detail);
        //tvStatus = requireActivity().findViewById(R.id.tv_prodStatus);
        tvProdNameP = view.findViewById(R.id.tv_prodNameP);
        tvProdBrandP = view.findViewById(R.id.tv_prodBrandP);
        tvCarboP = view.findViewById(R.id.tv_carbo);
        tvFatsP = view.findViewById(R.id.tv_fats);
        tvSugarsP = view.findViewById(R.id.tv_sugars);
        tvSaltP = view.findViewById(R.id.tv_salt);
        tvProteinsP = view.findViewById(R.id.tv_proteins);
        tvFiberP = view.findViewById(R.id.tv_fiber);
        //btnGraphs = requireActivity().findViewById(R.id.btn_graphs);

        tvProdNameP.setText(prodName);
        if (prodBrand.isEmpty()) {
            tvProdBrandP.setVisibility(View.GONE);
        }else{
            tvProdBrandP.setText(prodBrand);

        }
        //tvProdIngredients.setText(prodIngredients);
        if (prodCarbo.isEmpty()) {
            tvCarboP.setText(R.string.product_no_data);
            tvCarboP.setTextSize(14);
            tvCarboP.setTypeface(null, Typeface.NORMAL);
        }else{
            tvCarboP.setText(String.format("%s%s", prodCarbo, prodCarboUnit));
        }
        if (prodFiber.isEmpty()) {
            tvFiberP.setText(R.string.product_no_data);
            tvFiberP.setTextSize(14);
            tvFiberP.setTypeface(null, Typeface.NORMAL);
        }else {
            tvFiberP.setText(String.format("%s%s", prodFiber, prodFiberUnit));
        }
        if (prodProteins.isEmpty()) {
            tvProteinsP.setText(R.string.product_no_data);
            tvProteinsP.setTextSize(14);
            tvProteinsP.setTypeface(null, Typeface.NORMAL);
        }else {
            tvProteinsP.setText(String.format("%s%s", prodProteins, prodProteinsUnit));
        }
        if (prodFats.isEmpty()) {
            tvFatsP.setText(R.string.product_no_data);
            tvFatsP.setTextSize(14);
            tvFatsP.setTypeface(null, Typeface.NORMAL);
        }else{
            tvFatsP.setText(String.format("%s%s", prodFats, prodFatsUnit));
        }
        if (prodSugars.isEmpty()) {
            tvSugarsP.setText(R.string.product_no_data);
            tvSugarsP.setTextSize(14);
            tvSugarsP.setTypeface(null, Typeface.NORMAL);
        }else{
            tvSugarsP.setText(String.format("%s%s", prodSugars, prodSugarsUnit));
        }
        if (prodSalt.isEmpty()) {
            tvSaltP.setText(R.string.product_no_data);
            tvSaltP.setTextSize(14);
            tvSaltP.setTypeface(null, Typeface.NORMAL);
        }else {
            tvSaltP.setText(String.format("%s%s", prodSalt, prodSaltUnit));
        }

    }
}