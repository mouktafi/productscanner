package com.app.productscanner

import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.budiyev.android.codescanner.*

private const val CAMERA_REQUEST_CODE = 101;

class CodeScannerActivity : AppCompatActivity() {

    var progressDialog: ProgressDialog? = null

    private lateinit var codeScanner: CodeScanner

    private lateinit var scannerView: CodeScannerView
    private lateinit var resultScan: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_code_scanner)
        dialogNetwork()
        setupPermission()
        codeScanner()
    }

    private fun codeScanner(){
        scannerView = findViewById<CodeScannerView>(R.id.scanner_view)
        resultScan = findViewById<TextView>(R.id.tv_result_scan)
        codeScanner = CodeScanner(this, scannerView)
        codeScanner.apply {
            camera = CodeScanner.CAMERA_BACK
            formats = CodeScanner.ALL_FORMATS
            autoFocusMode = AutoFocusMode.SAFE
            scanMode = ScanMode.CONTINUOUS
            isAutoFocusEnabled = true
            isFlashEnabled = false

            decodeCallback = DecodeCallback {
                runOnUiThread {
                    resultScan.text = it.text
                    if (isNetworkAvailable()) {
                        goResultCode()
                    }
                }

            }
            errorCallback = ErrorCallback {
                runOnUiThread {
                    Log.e("main", "Camera initialization error: ${it.message}")
                }
            }
        }
        scannerView.setOnClickListener {
            codeScanner.startPreview()
        }
    }

    fun goResultCode() {
        val i = Intent(this, DetailActivity::class.java)
        i.putExtra("barcode", resultScan.text.toString())
        this.startActivity(i)
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onRestart() {
        super.onRestart()
        if (!isNetworkAvailable()) {
            dialogNetwork()
        }
    }

    override fun onPause() {
        progressDialog!!.cancel()
        codeScanner.releaseResources()
        super.onPause()
        resultScan.text = ""
    }

    override fun onStop() {
        super.onStop()
    }

    private fun setupPermission() {
        val permission:Int = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
        }
    }
    private fun makeRequest() {
        ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA), CAMERA_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Dai permessi alla camera", Toast.LENGTH_SHORT).show()
                } else {
                    //successful
                }
            }
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val cm = (getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager)
        val wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
        val mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
        return wifi != null && wifi.isConnected || mobile != null && mobile.isConnected
    }

    fun dialogNetwork() {
        progressDialog = ProgressDialog(this)
        if (!isNetworkAvailable()) {
            progressDialog!!.setTitle("Rete assente")
            progressDialog!!.setMessage("Connettere il dispositivo ad una rete")
            progressDialog!!.setCancelable(true)
            progressDialog!!.show()
        } else {
            progressDialog!!.cancel()
        }
    }
}
