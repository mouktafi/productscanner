package com.app.productscanner;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class UserEditActivity extends AppCompatActivity {

    ImageView ivUserSex;

    SharedPreferences spUserSex,spUserWeight,spUserBirthdate,spUserFit,spFitSpinnerPosition,spUserCalories;

    RadioButton rbUserSexM,rbUserSexF,rbUserSexNot;

    int selectedId;
    int userSexInt;
    int userWeightInt;
    int fitPositionInt;
    long userBirthdateLng;

    Date birthDateTst;
    DateFormat formatter;

    String userWeightStr,userBirthdateStr,userFitStr;

    EditText etUserWeight,etUserBirthdate;

    Spinner spnUserFit;

    final Calendar myCalendar= Calendar.getInstance();

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit);


        rbUserSexM = findViewById(R.id.radioM);
        rbUserSexF = findViewById(R.id.radioF);
        //rbUserSexNot = findViewById(R.id.radioNot);
        ivUserSex = findViewById(R.id.iv_user_sex);
        etUserWeight = findViewById(R.id.et_user_weight);
        etUserBirthdate = (EditText) findViewById(R.id.et_user_birthdate);
        spnUserFit = (Spinner) findViewById(R.id.sp_user_fit);

        spUserSex = this.getSharedPreferences("userSex", Context.MODE_PRIVATE);
        userSexInt = spUserSex.getInt("userSex", userSexInt);

        spUserWeight = this.getSharedPreferences("userWeight", Context.MODE_PRIVATE);
        userWeightInt = spUserWeight.getInt("userWeight", userWeightInt);

        spUserBirthdate = this.getSharedPreferences("userBirthdateStr", Context.MODE_PRIVATE);
        userBirthdateStr = spUserBirthdate.getString("userBirthdateStr", userBirthdateStr);
        //Log.i("localDate", userBirthdateStr);
        spUserBirthdate = this.getSharedPreferences("userBirthdateLng", Context.MODE_PRIVATE);

        spUserFit = this.getSharedPreferences("userFitStr", Context.MODE_PRIVATE);
        userFitStr = spUserFit.getString("userFitStr", userFitStr);

        spFitSpinnerPosition = this.getSharedPreferences("fitnessPosition", Context.MODE_PRIVATE);
        fitPositionInt = spFitSpinnerPosition.getInt("fitnessPosition", fitPositionInt);

        userWeightStr = String.valueOf(userWeightInt);
        etUserWeight.setText(userWeightStr);
        etUserBirthdate.setText(userBirthdateStr);
        spnUserFit.setSelection(fitPositionInt);

        if (userSexInt == 1) {
            rbUserSexM.setChecked(true);
        } else if (userSexInt == 2) {
            rbUserSexF.setChecked(true);
        } else {
            rbUserSexM.setChecked(true);
        }

        DatePickerDialog.OnDateSetListener date =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,day);
                updateLabel();
            }
        };
        etUserBirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(UserEditActivity.this,date,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }
    private void updateLabel(){
        String myFormat="dd MM yyyy";
        SimpleDateFormat dateFormat=new SimpleDateFormat(myFormat, Locale.US);
        etUserBirthdate.setText(dateFormat.format(myCalendar.getTime()));
    }

   @Override
    protected void onStop() {
        super.onStop();
       RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGrp);
       // get selected radio button from radioGroup
       selectedId = radioGroup.getCheckedRadioButtonId();

       // find the radiobutton by returned id
       RadioButton radioButton = (RadioButton) findViewById(selectedId);

       spUserSex = this.getSharedPreferences("userSex", Context.MODE_PRIVATE);
       spUserWeight = this.getSharedPreferences("userWeight", Context.MODE_PRIVATE);
       spUserBirthdate = this.getSharedPreferences("userBirthdateStr", Context.MODE_PRIVATE);
       spUserFit = this.getSharedPreferences("userFitStr", Context.MODE_PRIVATE);
       spFitSpinnerPosition = this.getSharedPreferences("fitnessPosition", Context.MODE_PRIVATE);
       spUserCalories = this.getSharedPreferences("userCalories", Context.MODE_PRIVATE);

       userWeightStr = etUserWeight.getText().toString();
       if(!TextUtils.isEmpty(userWeightStr)) {
           userWeightInt = Integer.parseInt(userWeightStr);
       }

       userBirthdateStr = etUserBirthdate.getText().toString();

       if(!TextUtils.isEmpty(userBirthdateStr)) {

           formatter = new SimpleDateFormat("dd MM yyyy");
               try {
                   birthDateTst = (Date)formatter.parse(userBirthdateStr);
               } catch (ParseException e) {
                   e.printStackTrace();
               }
           assert birthDateTst != null;
           userBirthdateLng = birthDateTst.getTime();
       }

       try {

           if (radioButton.getText() == null) {
               Log.i("onStop", (String) radioButton.getText());
               userSexInt = 0;
           }
           else if (radioButton.getText().equals("N.S.")) {
               userSexInt = 0;
           }
           else if (radioButton.getText().equals("Male")) {
                userSexInt = 1;
            }
           else if (radioButton.getText().equals("Female")) {
               userSexInt = 2;
           }

            userFitStr = spnUserFit.getSelectedItem().toString();
           fitPositionInt = spnUserFit.getSelectedItemPosition();

            spUserSex.edit().putInt("userSex", userSexInt).apply();
            spUserWeight.edit().putInt("userWeight", userWeightInt).apply();
            spUserBirthdate.edit().putString("userBirthdateStr",userBirthdateStr).apply();
            spUserFit.edit().putString("userFitStr",userFitStr).apply();
            spFitSpinnerPosition.edit().putInt("fitnessPosition", fitPositionInt).apply();



       } catch (Exception e) {
           e.printStackTrace();
       }

    }
}