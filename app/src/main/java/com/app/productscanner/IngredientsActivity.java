package com.app.productscanner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class IngredientsActivity extends AppCompatActivity {

    Bundle bundle;
    String ingredients;
    TextView tvIngredients;
    ImageView ivNutriscore;
    ImageView ivNova;
    ImageView ivEcoscore;
    ListView lvIngredients;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredients);
        bundle = getIntent().getExtras();
        String prodName = bundle.getString("prodName");
        //String prodIngredients = bundle.getString("prodIngredients");
        String prodNutriscore = bundle.getString("prodNutriscore");
        String prodNova = bundle.getString("prodNova");
        String prodEcoscore = bundle.getString("prodEcoscore");
        String ingredients_str = bundle.getString("ingredients");
        Log.i("ecoscore", prodEcoscore);
        setTitle(prodName);

        //tvIngredients = findViewById(R.id.tv_ingredients);
        //tvIngredients.setText(ingredients);
        lvIngredients = findViewById(R.id.lv_ingredients);
        ivNutriscore = findViewById(R.id.iv_nutriscore);
        ivNova = findViewById(R.id.iv_nova);
        ivEcoscore = findViewById(R.id.iv_ecoscore);

        JSONArray ingredientsArray = null;
        try {
            ingredientsArray = new JSONArray(ingredients_str);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        int length = ingredientsArray.length();
        //Log.i("ingArr", String.valueOf(ingObj));
        List<String> listContents = new ArrayList<String>(length);
        for (int i = 0; i< Objects.requireNonNull(ingredientsArray).length(); i++){
            JSONObject ingredientObj = null;
            try {
                ingredientObj = ingredientsArray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            assert ingredientObj != null;
            ingredients = ingredientObj.optString("text").toUpperCase(Locale.ROOT);
            Log.i("ingredients", ingredients);
            listContents.add(ingredients);
            lvIngredients.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, listContents));
        }


        if (prodNutriscore.isEmpty()) {
            ivNutriscore.setVisibility(View.INVISIBLE);
        }else if (prodNutriscore.equals("a")) {
            ivNutriscore.setImageResource(R.drawable.nutriscore_a);
        }else if (prodNutriscore.equals("b")) {
            ivNutriscore.setImageResource(R.drawable.nutriscore_b);
        }else if (prodNutriscore.equals("c")) {
            ivNutriscore.setImageResource(R.drawable.nutriscore_c);
        }else if (prodNutriscore.equals("d")) {
            ivNutriscore.setImageResource(R.drawable.nutriscore_d);
        }else if (prodNutriscore.equals("e")) {
            ivNutriscore.setImageResource(R.drawable.nutriscore_e);
        }

        if (prodNova.isEmpty()) {
            ivNova.setImageResource(R.drawable.nova_group_unknown);
        }else if (prodNova.equals("1")) {
            ivNova.setImageResource(R.drawable.nova_group_1);
        }else if (prodNova.equals("2")) {
            ivNova.setImageResource(R.drawable.nova_group_2);
        }else if (prodNova.equals("3")) {
            ivNova.setImageResource(R.drawable.nova_group_3);
        }else if (prodNova.equals("4")) {
            ivNova.setImageResource(R.drawable.nova_group_4);
        }

        if (prodEcoscore.isEmpty()) {
            ivEcoscore.setVisibility(View.INVISIBLE);
        }else if (prodEcoscore.equals("a")) {
            ivEcoscore.setImageResource(R.drawable.ecoscore_a);
        }else if (prodEcoscore.equals("b")) {
            ivEcoscore.setImageResource(R.drawable.ecoscore_b);
        }else if (prodEcoscore.equals("c")) {
            ivEcoscore.setImageResource(R.drawable.ecoscore_c);
        }else if (prodEcoscore.equals("d")) {
            ivEcoscore.setImageResource(R.drawable.ecoscore_d);
        }else if (prodEcoscore.equals("e")) {
            ivEcoscore.setImageResource(R.drawable.ecoscore_e);
        }else if (prodEcoscore.equals("unknown")) {
            ivEcoscore.setImageResource(R.drawable.ecoscore_unknown);
        }
    }
}