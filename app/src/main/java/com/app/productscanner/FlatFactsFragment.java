package com.app.productscanner;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class FlatFactsFragment extends Fragment {

    String url,urlImg,barcode,spBarcode,prodName,prodBrand,prodCarbo,prodCarboUnit,prodCarboSrv,prodFiber,prodFiberUnit,prodProteins,prodProteinsUnit,prodFats,prodFatsUnit,prodSugars,prodSugarsUnit,prodSalt,prodSaltUnit,prodIngredients,prodNutriscore,prodNova,prodEcoscore,ingredients_str;
    ConstraintLayout clStatus,clDetail;
    TextView tvStatus,tvProdNameF,tvProdBrandF,tvCarboF,tvFiberF,tvFatsF,tvProteinsF,tvSugarsF,tvSaltF;
    ImageView ivProdImage;
    Button btnGraphs,btnAltFacts;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        prodName = getArguments().getString("prodName");
        prodBrand = getArguments().getString("prodBrand");
        prodCarbo = getArguments().getString("prodCarbo");
        prodCarboUnit = getArguments().getString("prodCarboUnit");
        prodFiber = getArguments().getString("prodFiber");
        prodFiberUnit = getArguments().getString("prodFiberUnit");
        prodProteins = getArguments().getString("prodProteins");
        prodProteinsUnit = getArguments().getString("prodProteinsUnit");
        prodFats = getArguments().getString("prodFats");
        prodFatsUnit = getArguments().getString("prodFatsUnit");
        prodSugars = getArguments().getString("prodSugars");
        prodSugarsUnit = getArguments().getString("prodSugarsUnit");
        prodSalt = getArguments().getString("prodSalt");
        prodSaltUnit = getArguments().getString("prodSaltUnit");

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_flat_facts, container, false);
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //clStatus = requireActivity().findViewById(R.id.cl_status;
        clDetail = requireActivity().findViewById(R.id.cl_detail);
        //tvStatus = requireActivity().findViewById(R.id.tv_prodStatus);
        tvProdNameF = view.findViewById(R.id.tv_prodNameP);
        tvProdBrandF = view.findViewById(R.id.tv_prodBrandP);
        //tvProdIngredients = findViewById(R.id.tv_prodIngredients);
        tvCarboF = view.findViewById(R.id.tv_carbo);
        tvFatsF = view.findViewById(R.id.tv_fats);
        tvSugarsF = view.findViewById(R.id.tv_sugars);
        tvSaltF = view.findViewById(R.id.tv_salt);
        tvProteinsF = view.findViewById(R.id.tv_proteins);
        tvFiberF = view.findViewById(R.id.tv_fiber);
        //ivProdImageF = view.findViewById(R.id.iv_prodimage);
        //btnGraphs = requireActivity().findViewById(R.id.btn_graphs);
        //btnAltFacts = requireActivity().findViewById(R.id.btn_alt_facts);

        tvProdNameF.setText(prodName);
        if (prodBrand.isEmpty()) {
            tvProdBrandF.setVisibility(View.GONE);
        }else{
            tvProdBrandF.setText(prodBrand);
        }
        //tvProdIngredients.setText(prodIngredients);
        if (prodCarbo.isEmpty()) {
            tvCarboF.setText(R.string.product_no_data);
            tvCarboF.setTextSize(14);
            tvCarboF.setTypeface(null, Typeface.NORMAL);
        }else{
            tvCarboF.setText(String.format("%s%s", prodCarbo, prodCarboUnit));
        }
        if (prodFiber.isEmpty()) {
            tvFiberF.setText(R.string.product_no_data);
            tvFiberF.setTextSize(14);
            tvFiberF.setTypeface(null, Typeface.NORMAL);
        }else {
            tvFiberF.setText(String.format("%s%s", prodFiber, prodFiberUnit));
        }
        if (prodProteins.isEmpty()) {
            tvProteinsF.setText(R.string.product_no_data);
            tvProteinsF.setTextSize(14);
            tvProteinsF.setTypeface(null, Typeface.NORMAL);
        }else {
            tvProteinsF.setText(String.format("%s%s", prodProteins, prodProteinsUnit));
        }
        if (prodFats.isEmpty()) {
            tvFatsF.setText(R.string.product_no_data);
            tvFatsF.setTextSize(14);
            tvFatsF.setTypeface(null, Typeface.NORMAL);
        }else{
            tvFatsF.setText(String.format("%s%s", prodFats, prodFatsUnit));
        }
        if (prodSugars.isEmpty()) {
            tvSugarsF.setText(R.string.product_no_data);
            tvSugarsF.setTextSize(14);
            tvSugarsF.setTypeface(null, Typeface.NORMAL);
        }else {
            tvSugarsF.setText(String.format("%s%s", prodSugars, prodSugarsUnit));
        }
        if (prodSalt.isEmpty()) {
            tvSaltF.setText(R.string.product_no_data);
            tvSaltF.setTextSize(14);
            tvSaltF.setTypeface(null, Typeface.NORMAL);
        }else {
            tvSaltF.setText(String.format("%s%s", prodSalt, prodSaltUnit));
        }

    }
}